### Pulsar Aitoff Projection Plotter ###

## Requirements ##

Prerequisites are listed in requirements.txt

Install these with

```
python -m pip install --user -r requirements.txt
``` 

## Usage ##

```
python plot_aitoff.py -p <pulsar> -f <output filename>
```

The program will save a png of the Milky Way to the working directory.

![Image Description](B0531+21_aitoff.png)
