numpy==1.17.4
psrqpy==1.0.11
astropy==4.0.1.post1
galpy==1.6.0.post0
mw_plot==0.6.0
