#!/usr/bin/env python

import psrqpy
from astropy import units as  u
import astropy.coordinates as apycoords
from galpy.orbit import Orbit
from mw_plot import MWSkyMap

import numpy as np
import argparse
from sys import exit

class aitoffPlotter(object):

    def __init__(self, pulsar=None, outfile=None):
        self.pulsar = pulsar
        self.outfile = outfile

    @staticmethod
    def _get_pos_pulsar(pulsar):
        try:
            query_ra = psrqpy.QueryATNF(params=['RAJD', 'DECJD'], psrs=[pulsar])
            results_ra = query_ra.table['RAJD']
            results_dec = query_ra.table['DECJD']
            ra = float(np.asarray(results_ra)[0]) * u.degree
            dec = float(np.asarray(results_dec)[0]) * u.degree

            return ra, dec
        except KeyError:
            print("{} not found in database".format(pulsar))
            return None, None

    def plot(self):

        plot_instance = MWSkyMap(projection='aitoff', grayscale=False)
        plot_instance.imalpha = 1.

        if self.pulsar:

            # Set up plot instance
            plot_instance.s = 200
            plot_instance.title = str(self.pulsar)

            # Get RA and DEC of pulsar from psrcat
            this_ra, this_dec = self._get_pos_pulsar(self.pulsar)
            if not this_ra:
                exit(9)
                
            plot_instance.mw_scatter(this_ra, this_dec, 'r')
            
            if not self.outfile:
                this_outfile = str(self.pulsar) + "_aitoff.png"  
            else:
                this_outfile = str(self.outfile) + ".png"
            plot_instance.savefig(file=this_outfile)
            print("{} written to disk".format(this_outfile))

def main():

    parser = argparse.ArgumentParser(description='Pulsar Aitoff Projection Plotter')
    parser.add_argument('-p', '--pulsar', help='Pulsar', required=False, type=str)
    parser.add_argument('-o', '--outfile', help='Output image file name (png)', required=False, type=str)
    args = parser.parse_args()

    plotter = aitoffPlotter(args.pulsar, args.outfile)
    plotter.plot()

if __name__ == '__main__':
    main()

